# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-mobile package.
#
# SPDX-FileCopyrightText: 2023, 2024 Kristof Kiszel <kiszel.kristof@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-mobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-09 00:39+0000\n"
"PO-Revision-Date: 2024-02-11 20:35+0100\n"
"Last-Translator: Kristof Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#: dragstate.cpp:14
#, kde-format
msgid "Folder"
msgstr "Mappa"

#: package/contents/ui/AppDrawerHeader.qml:27
#, kde-format
msgid "Applications"
msgstr "Alkalmazások"

#: package/contents/ui/FavouritesBar.qml:146
#: package/contents/ui/FavouritesBar.qml:220
#: package/contents/ui/FolderView.qml:269
#: package/contents/ui/HomeScreenPage.qml:205
#: package/contents/ui/HomeScreenPage.qml:285
#, kde-format
msgid "Remove"
msgstr "Eltávolítás"

#: package/contents/ui/settings/AppletListViewer.qml:56
#, kde-format
msgid "Widgets"
msgstr "Elemek"

#: package/contents/ui/settings/SettingsWindow.qml:78
#, kde-format
msgid "Homescreen Settings"
msgstr "Kezdőképernyő beállításai"

#: package/contents/ui/settings/SettingsWindow.qml:87
#, kde-format
msgid "Icons"
msgstr "Ikonok"

#: package/contents/ui/settings/SettingsWindow.qml:118
#, kde-format
msgid "Number of rows"
msgstr "Sorok száma"

#: package/contents/ui/settings/SettingsWindow.qml:119
#, kde-format
msgid "Number of columns"
msgstr "Oszlopok száma"

#: package/contents/ui/settings/SettingsWindow.qml:123
#, kde-format
msgid "Size of icons on homescreen"
msgstr "A kezdőképernyő ikonjainak mérete"

#: package/contents/ui/settings/SettingsWindow.qml:162
#, kde-format
msgid "The rows and columns will swap depending on the screen rotation."
msgstr "A sorok és oszlopok felcserélődnek a képernyő forgásától függően."

#: package/contents/ui/settings/SettingsWindow.qml:166
#, kde-format
msgid "Homescreen"
msgstr "Kezdőképernyő"

#: package/contents/ui/settings/SettingsWindow.qml:172
#, kde-format
msgid "Show labels on homescreen"
msgstr "Címkék megjelenítése a kezdőképernyőn"

#: package/contents/ui/settings/SettingsWindow.qml:185
#, kde-format
msgid "Show labels in favorites bar"
msgstr "Címkék megjelenítése a kedvencek sávon"

#: package/contents/ui/settings/SettingsWindow.qml:198
#, kde-format
msgid "Page transition effect"
msgstr "Oldalátmenet hatás"

#: package/contents/ui/settings/SettingsWindow.qml:204
#, kde-format
msgid "Slide"
msgstr "Csúszás"

#: package/contents/ui/settings/SettingsWindow.qml:205
#, kde-format
msgid "Cube"
msgstr "Kocka"

#: package/contents/ui/settings/SettingsWindow.qml:206
#, kde-format
msgid "Fade"
msgstr "Elhalványulás"

#: package/contents/ui/settings/SettingsWindow.qml:207
#, kde-format
msgid "Stack"
msgstr "Kupac"

#: package/contents/ui/settings/SettingsWindow.qml:208
#, kde-format
msgid "Rotation"
msgstr "Forgatás"

#: package/contents/ui/settings/SettingsWindow.qml:223
#, kde-format
msgid "Favorites Bar"
msgstr "Kedvencek sáv"

#: package/contents/ui/settings/SettingsWindow.qml:240
#, kde-format
msgctxt "@title:group settings group"
msgid "Wallpaper"
msgstr "Háttérkép"

#: package/contents/ui/settings/SettingsWindow.qml:246
#, kde-format
msgctxt "@option:check"
msgid "Show wallpaper blur effect"
msgstr "Háttérkép-elmosási effektus megjelenítése"

#: package/contents/ui/settings/SettingsWindow.qml:257
#, kde-format
msgid "General"
msgstr "Általános"

#: package/contents/ui/settings/SettingsWindow.qml:264
#, kde-format
msgctxt "@action:button"
msgid "Switch between homescreens and more wallpaper options"
msgstr "Váltás a kezdőképernyők között és további háttérkép beállítások"

#: package/contents/ui/settings/SettingsWindow.qml:291
#, kde-format
msgid "Export layout to"
msgstr "Elrendezés exportálása ide"

#: package/contents/ui/settings/SettingsWindow.qml:310
#, kde-format
msgid "Import layout from"
msgstr "Elrendezés importálása innen"

#: package/contents/ui/settings/SettingsWindow.qml:321
#: package/contents/ui/settings/SettingsWindow.qml:328
#, kde-format
msgid "Export Status"
msgstr "Exportálás állapota"

#: package/contents/ui/settings/SettingsWindow.qml:322
#, kde-format
msgid "Failed to export to %1"
msgstr "Nem sikerült exportálni ide: %1"

#: package/contents/ui/settings/SettingsWindow.qml:329
#, kde-format
msgid "Homescreen layout exported successfully to %1"
msgstr "A kezdőképernyő elrendezése sikeresen exportálva ide: %1"

#: package/contents/ui/settings/SettingsWindow.qml:335
#, kde-format
msgid "Confirm Import"
msgstr "Importálás megerősítése"

#: package/contents/ui/settings/SettingsWindow.qml:336
#, kde-format
msgid "This will overwrite your existing homescreen layout!"
msgstr "Ezzel felülírja a kezdőképernyője elrendezését!"
