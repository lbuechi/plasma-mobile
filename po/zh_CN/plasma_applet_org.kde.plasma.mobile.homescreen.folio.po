msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-09 00:39+0000\n"
"PO-Revision-Date: 2024-02-05 11:37\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/plasma-mobile/plasma_applet_org.kde."
"plasma.mobile.homescreen.folio.pot\n"
"X-Crowdin-File-ID: 48272\n"

#: dragstate.cpp:14
#, kde-format
msgid "Folder"
msgstr "文件夹"

#: package/contents/ui/AppDrawerHeader.qml:27
#, kde-format
msgid "Applications"
msgstr "应用程序"

#: package/contents/ui/FavouritesBar.qml:146
#: package/contents/ui/FavouritesBar.qml:220
#: package/contents/ui/FolderView.qml:269
#: package/contents/ui/HomeScreenPage.qml:205
#: package/contents/ui/HomeScreenPage.qml:285
#, kde-format
msgid "Remove"
msgstr "移除"

#: package/contents/ui/settings/AppletListViewer.qml:56
#, kde-format
msgid "Widgets"
msgstr "挂件"

#: package/contents/ui/settings/SettingsWindow.qml:78
#, kde-format
msgid "Homescreen Settings"
msgstr "主屏幕设置"

#: package/contents/ui/settings/SettingsWindow.qml:87
#, kde-format
msgid "Icons"
msgstr "图标"

#: package/contents/ui/settings/SettingsWindow.qml:118
#, kde-format
msgid "Number of rows"
msgstr "行数"

#: package/contents/ui/settings/SettingsWindow.qml:119
#, kde-format
msgid "Number of columns"
msgstr "列数"

#: package/contents/ui/settings/SettingsWindow.qml:123
#, kde-format
msgid "Size of icons on homescreen"
msgstr "主屏幕的图标大小"

#: package/contents/ui/settings/SettingsWindow.qml:162
#, kde-format
msgid "The rows and columns will swap depending on the screen rotation."
msgstr "行和列将根据屏幕旋转进行对换。"

#: package/contents/ui/settings/SettingsWindow.qml:166
#, kde-format
msgid "Homescreen"
msgstr "主屏幕"

#: package/contents/ui/settings/SettingsWindow.qml:172
#, kde-format
msgid "Show labels on homescreen"
msgstr "主屏幕图标显示标签"

#: package/contents/ui/settings/SettingsWindow.qml:185
#, kde-format
msgid "Show labels in favorites bar"
msgstr "收藏栏图标显示标签"

#: package/contents/ui/settings/SettingsWindow.qml:198
#, kde-format
msgid "Page transition effect"
msgstr "页面切换特效"

#: package/contents/ui/settings/SettingsWindow.qml:204
#, kde-format
msgid "Slide"
msgstr "滑动"

#: package/contents/ui/settings/SettingsWindow.qml:205
#, kde-format
msgid "Cube"
msgstr "立方"

#: package/contents/ui/settings/SettingsWindow.qml:206
#, kde-format
msgid "Fade"
msgstr "渐隐渐显"

#: package/contents/ui/settings/SettingsWindow.qml:207
#, kde-format
msgid "Stack"
msgstr "堆叠"

#: package/contents/ui/settings/SettingsWindow.qml:208
#, kde-format
msgid "Rotation"
msgstr "旋转"

#: package/contents/ui/settings/SettingsWindow.qml:223
#, kde-format
msgid "Favorites Bar"
msgstr "收藏栏"

#: package/contents/ui/settings/SettingsWindow.qml:240
#, kde-format
msgctxt "@title:group settings group"
msgid "Wallpaper"
msgstr ""

#: package/contents/ui/settings/SettingsWindow.qml:246
#, kde-format
msgctxt "@option:check"
msgid "Show wallpaper blur effect"
msgstr ""

#: package/contents/ui/settings/SettingsWindow.qml:257
#, kde-format
msgid "General"
msgstr "常规"

#: package/contents/ui/settings/SettingsWindow.qml:264
#, kde-format
msgctxt "@action:button"
msgid "Switch between homescreens and more wallpaper options"
msgstr ""

#: package/contents/ui/settings/SettingsWindow.qml:291
#, kde-format
msgid "Export layout to"
msgstr "导出布局到"

#: package/contents/ui/settings/SettingsWindow.qml:310
#, kde-format
msgid "Import layout from"
msgstr "导入布局来源"

#: package/contents/ui/settings/SettingsWindow.qml:321
#: package/contents/ui/settings/SettingsWindow.qml:328
#, kde-format
msgid "Export Status"
msgstr "导出状态"

#: package/contents/ui/settings/SettingsWindow.qml:322
#, kde-format
msgid "Failed to export to %1"
msgstr "导出到 %1 失败"

#: package/contents/ui/settings/SettingsWindow.qml:329
#, kde-format
msgid "Homescreen layout exported successfully to %1"
msgstr "主屏幕布局成功导出到 %1"

#: package/contents/ui/settings/SettingsWindow.qml:335
#, kde-format
msgid "Confirm Import"
msgstr "确认导入"

#: package/contents/ui/settings/SettingsWindow.qml:336
#, kde-format
msgid "This will overwrite your existing homescreen layout!"
msgstr "这将覆盖您现有的主屏幕布局！"
